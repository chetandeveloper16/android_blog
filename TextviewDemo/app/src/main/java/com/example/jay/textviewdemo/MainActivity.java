package com.example.jay.textviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    //First we have to create the object of textivew control
    TextView t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Then we have to bind with ID
        t1=findViewById(R.id.txt1);


        //Set the text into the textview
        t1.setText("Good Morning");

    }
}
